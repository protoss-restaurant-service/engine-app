package com.engine.service;

import com.engine.entity.LineData;
import com.engine.entity.MasterDataDetail;

import java.util.List;

public interface AppMailDataService {

    List<MasterDataDetail> masterDatakey(Long id, String code);
    List<LineData> getUserId();
    void SaveByTableId(String tableId);
    void SaveByJsonCus(String json);
    public boolean checkTextMatches(String str);
//    ResponseEntity<String> appby(String json);

}
