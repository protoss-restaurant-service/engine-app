package com.engine.repository;

import com.engine.entity.LineData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface LineDataRepository extends JpaSpecificationExecutor<LineData>,
        JpaRepository<LineData, Long>,
        PagingAndSortingRepository<LineData, Long> {

        }
