package com.engine.controller;

import com.engine.entity.LineData;
import com.engine.entity.MasterDataDetail;
import com.engine.repository.LineDataRepository;
import com.engine.service.AppMailDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/Dashboard")
public class AppMailDataController {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AppMailDataService appMailDataService;

    @Autowired
    private LineDataRepository lineDataRepository;

    @GetMapping("/getkeyword")
    public List<MasterDataDetail> masterDatakey(@RequestParam("id") Long id, @RequestParam("code") String code) {
        return appMailDataService.masterDatakey(id, code);
    }

    @PostMapping("/saveByJsonCus")
    public void SaveByJsonCus(@RequestBody String json){
        appMailDataService.SaveByJsonCus(json);
    }

    @GetMapping("/getuserid")
    public List<LineData> getUserId(){
        return appMailDataService.getUserId();
    }

    @GetMapping("/checkTextMatches")
    public boolean checkTextMatches(@RequestParam("str") String str){
        return appMailDataService.checkTextMatches(str);
    }

}

